#include <iostream>
#include "Questbook.h"

int main()
{
	Questbook::Init("xml/quest.xml");
	ColliderE col;
	col.flags = PICKEDUP;
	ColliderEn cole;
	cole.flags = PICKEDUP;
	Event message;
	message.data = (void*)&col;
	message.type = ColliderEmpty;
	Questbook::Update(message);

	while (true)
	{
		Questbook::Update(message);
		Questbook::Draw();
		message.data = (void*)&cole;
		message.type = ColliderEnter;
		system("pause");

	}

	system("pause");
	return 0;
}