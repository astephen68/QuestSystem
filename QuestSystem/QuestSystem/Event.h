#pragma once
enum InventoryFlags
{
	PICKEDUP = 0,
	DROPPED = 1,
	THROWN = 2
};

enum EventTypes
{
	InventoryTask,
	QuestType,
	AreaEntered,
	FetchTask,
	KillTask,
	RepairTask,
	ColliderEmpty,
	InventoryPickup,
	ColliderEnter
};


struct Inventory
{
	Inventory() {};
	InventoryFlags flags;
	//inventory item
	~Inventory(){};
};

struct ColliderE
{
	InventoryFlags flags;
	
};

struct ColliderEn
{
	InventoryFlags flags;
};



class Event
{
public:
	Event();
	~Event();
	EventTypes type;
	template <typename T>
	T GetData();
	void* data;


};

template <typename T>
T  Event::GetData()
{
	return *(reinterpret_cast<T*>(data));
}